const checkPhoneNumber = (phone) => {
	const regex = /^(06|07|01)[0-9]{8}$/;
	return !!phone.match(regex);
}

console.log(checkPhoneNumber("0643465748"));
console.log(checkPhoneNumber("0843467748"));
console.log(checkPhoneNumber("064345774"));
