const checkPalindrome = (str) => str.split("").reverse().join("") === str;

console.log(checkPalindrome("sexes"))
console.log(checkPalindrome("hello"))