class drawBar {

	constructor(sum,nbr) {
		this.sum = sum;
		this.nbr = nbr;
		this.run();
	}

	run() {
		return document.querySelector("#app").innerHTML = `
			<label for="bar">Barre de chargement interactive</label>
			<progress id="bar" max="${this.sum}" value="${this.nbr}"></progress>
		`;
	}




}

const barre = new drawBar(100,70)