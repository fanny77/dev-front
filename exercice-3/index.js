class MyMorpionXO {

	constructor()
	{
		//On récupére l'emplacement de notre jeu dans la page html
		this.root = document.querySelector("#app");
		//On initialise le premier joueur
		this.currentPlayer = "X";
		//On initialise les scores
		this.score = {
			"X": 0,
			"O": 0
		};
		//On commence la partie
		this.startGame();
	}
	startGame()
	{
		//On crée une représentation virtuelle de la grille de jeu avec les cases non attribués à un joueur spécifique
		this.virtualGrid = [
			[null,null,null],
			[null,null,null],
			[null,null,null]
		];

		//On efface les données de la partie précédentes
		const table = this.root.querySelector("table");
		if (table !== null) {
			this.root.removeChild(table);
			this.root.removeChild(this.root.querySelector("button"));
			this.root.removeChild(this.root.querySelector("#gagnant"));
			this.root.removeChild(this.root.querySelector("#scores"));
		}

		this.render();
	}
	render()
	{
		//Création du tableau
		const table = document.createElement("table");
		//Ajout du tableau dans l'élement html
		this.root.appendChild(table);

		//Création des lignes du tableau
		for(let y = 0; y < 3; y++){
			let line = document.createElement("tr");
			table.appendChild(line);

			//Création des colonnes du tableau
			for(let x =0; x < 3; x++){
				let cell = document.createElement("td");
				line.appendChild(cell);

				//Ajout des coordonnées de la cellule en attribut data html
				cell.dataset.x = x;
				cell.dataset.y = y;

				//On ajoute l'évenement clic sur la cellule
				cell.onclick = this.onCellClick.bind(this);
			}
		}

		//Création d'une section déclarant le gagnant
		this.WinElement = document.createElement("div");
		this.WinElement.id = "gagnant";
		this.WinElement.innerText = "--";
		this.root.appendChild(this.WinElement);

		//Création d'une section affichage des scores
		this.scoresElement = document.createElement("div");
		this.scoresElement.id = "scores";
		this.scoresElement.innerText = `
		X a gagné : ${this.score["X"]} / O a gagné : ${this.score["O"]}`;
		//On affiche avant le premier enfant de l'élèment parent
		this.root.prepend(this.scoresElement);

	}

	onCellClick(ev)
	{
		//On déclare la cellule choisie
		let el = ev.currentTarget;
		//On change l'évenement clic à null pour ne pas pouvoir modifier le choix
		el.onclick = null;

		//On affiche le joueur en cours
		el.innerText = this.currentPlayer;

		//On modifie la grille virtuelle en fonction des coordonnées de la cellule cliquée en indiquant le joueur
		this.virtualGrid[el.dataset.y][el.dataset.x] = this.currentPlayer;
		//On passe au joueur suivant
		this.currentPlayer = (this.currentPlayer == "X") ? "O" : "X";

		//On vérifie si il y a un gagnant
		let winner = this.checkWinner();
		//On vérifie si la grille est complète
		let completedGride = this.checkCompleteGrid();
		if (winner !== null) {
			this.WinElement.innerText = winner + " a gagné";
			this.score[winner] += 1;
		}
		else if (completedGride == true) {
			this.WinElement.innerText = "Personne n'a gagné";
		}
		//Si il y a un gagnant on stop la partie
		if(winner !== null || completedGride)
			this.stopGame();
	}

	stopGame()
	{
		//On enlève la possibilité de cliquer sur toutes les cellules
		this.root.querySelectorAll("table td").forEach((cell) => {
			cell.onclick = null;
		});

		//On créer le bouton rejouer
		const restart = document.createElement("button");
		restart.innerText = "Rejouer";

		restart.addEventListener('click', () => {
			this.startGame();
		})

		this.root.appendChild(restart);
	}

	checkWinner()
	{
		//On vérifie les lignes en fonction de son contenu (joueur)
		for(let i = 0; i < 3; i++){
			if(this.virtualGrid[i][0] !== null && this.virtualGrid[i][0] == this.virtualGrid[i][1] && this.virtualGrid[i][1] == this.virtualGrid[i][2])
				return this.virtualGrid[i][0];

				//On vérifie les colonnes en fonction de son contenu (joueur)
			if(this.virtualGrid[0][i] !== null && this.virtualGrid[0][i] == this.virtualGrid[1][i] && this.virtualGrid[0][i] == this.virtualGrid[2][i])
				return this.virtualGrid[0][i];
		}

		//On vérifie les diagonales en fonction de son contenu (joueur)
		if(this.virtualGrid[1][1] !== null){
			if(this.virtualGrid[0][0] == this.virtualGrid[1][1] && this.virtualGrid[1][1] == this.virtualGrid[2][2])
				return this.virtualGrid[1][1];
			if(this.virtualGrid[0][2] == this.virtualGrid[1][1] && this.virtualGrid[1][1] == this.virtualGrid[2][0])
				return this.virtualGrid[1][1];
		}

		//Si aucun gagnant on retourne null
		return null;
	}

	checkCompleteGrid()
	{
		//On vérifie si toutes les cellules ont été complétées
		for (let i = 0; i < 3; i++) {
			for (let j = 0; j < 3 ; j++) {
				if (this.virtualGrid[i][j] === null)
				{
					return false;
				}
			}
		}
		return true;
	}
}

const morpionXO = new MyMorpionXO();
