this.el = document.querySelector("#app");
const form = document.createElement("div");

form.innerHTML =
`<form action="#" method="post">
<label for="nom">Votre nom</label>
<input type="text" name="nom" id="nom"><span class="error" id="errorNom"></span>
<label for="prenom">Votre prénom</label>
<input type="text" name="prenom" id="prenom"><span class="error" id="errorPrenom"></span>
<label for="mail">Votre E-mail</label>
<input type="text" name="mail" id="mail"><span class="error" id="errorMail"></span>
<label for="password">Votre mot de passe</label>
<input type="password" name="password" id="password"><span class="error" id="errorPassword"></span>
<input type="submit" value="Envoyer">
</form>"`
this.el.appendChild(form)


const verif = (regex,valeurs,message,id) => {
	if (valeurs.match(regex) == null) {
		document.querySelector(id).innerHTML = message;
	}
	else{
		document.querySelector(id).innerHTML="";
	}
}

document.querySelector("form").onsubmit = function(e) {
	e.preventDefault();

	//Récupérer les valeurs
	const nom = this.querySelector("#nom").value;
	const prenom = this.querySelector("#prenom").value;
	const mail = this.querySelector("#mail").value;
	const password = this.querySelector("#password").value;


	//Vérification Nom
	verif(/^\D+$/i,nom,"Merci de rentrer un nom valide","#errorNom");
	//Vérification Prénom
	verif(/^\D+$/i,prenom,"Merci de rentrer un prénom valide","#errorPrenom");
	//Vérification email format
	verif(/(\w)+@(\w)+\.(\w)+/i,mail,"Merci de rentrer un email valide","#errorMail");
	//Vérification longueur MDP
	verif(/.{8,}/gi,password,"Votre mot de passe doit faire plus de 8 caractère, contenir au moins une majuscule, une minuscule et un caractère spécial !@#$%^&*()--__+. ","#errorPassword");
	//Vérification présence chiffre MDP
	verif(/\d+/g,password,"Votre mot de passe doit faire plus de 8 caractère, contenir au moins une majuscule, une minuscule et un caractère spécial !@#$%^&*()--__+. ","#errorPassword");
	//Vérification présence majuscule MDP
	verif(/[A-Z]+/g,password,"Votre mot de passe doit faire plus de 8 caractère, contenir au moins une majuscule, une minuscule et un caractère spécial !@#$%^&*()--__+. ","#errorPassword");
	//Vérification présence minuscule MDP
	verif(/[a-z]+/g,password,"Votre mot de passe doit faire plus de 8 caractère, contenir au moins une majuscule, une minuscule et un caractère spécial !@#$%^&*()--__+. ","#errorPassword");
	//Vérification présence caractère spécial MDP
	verif(/[!@#$%^&*()--__+.]+/g,password,"Votre mot de passe doit faire plus de 8 caractère, contenir au moins une majuscule, une minuscule et un caractère spécial !@#$%^&*()--__+. ","#errorPassword");
}