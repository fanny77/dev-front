class Battleship
{
	//Initialisation de la liste des bateaux
	battleshipTypes = {
		1: 'Frégate',
		2: 'Croiseur',
		3: 'Sous-marin',
		4: 'Contre-torpilleur',
		5: 'Porte-avion'
	}

	constructor()
	{
		//Initialisation du plateau de jeu
		this.battleGrid = [
			[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
			[0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 4, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		];

		this.root = document.querySelector("#app");
		this.render();
	}

	render()
	{
		//Création du tableau
		const table = document.createElement("table");
		this.root.appendChild(table);

		const thead = document.createElement("thead");
		table.appendChild(thead);

		const th_corner = document.createElement("th");
		thead.appendChild(th_corner);

		this.battleGrid.forEach((line, y) => {

			const tr = document.createElement("tr");
			table.appendChild(tr);

			const th = document.createElement("th");
			tr.appendChild(th);
			th.innerText = "ABCDEFGHIJKL".split("")[y];

			line.forEach((cell, x) => {
				if(y == 0){
					const th = document.createElement("th");
					th.innerText = x+1;
					thead.appendChild(th);
				}

				const td = document.createElement("td");
				tr.appendChild(td);

				td.dataset.typeship = cell;

				//Ajout de l'évnement cliquer sur une cellule
				td.addEventListener('click', (ev) => {
					this.onCellClick(ev);
				});
			});

		});

		this.col_right = document.createElement("div");
		this.root.appendChild(this.col_right);

		//Création de la liste
		this.list = document.createElement("ul");
		this.col_right.appendChild(this.list);

		Object.keys(this.battleshipTypes).forEach(key => {

			const li = document.createElement("li");
			this.list.appendChild(li);

			li.id = "ship-" + key;
			li.innerText = this.battleshipTypes[key];

		});

		//Création du bandeau fin du jeu
		this.win_div = document.createElement("div");
		this.win_div.classList.add("message");
		this.col_right.appendChild(this.win_div);
	}

	onCellClick(ev)
	{
		//On stocke la cellule choisie et le type de la cellule
		const targetCell = ev.target;
		const cellType = targetCell.dataset.typeship;

		if(cellType == 0){
			// La case correspond à de l'eau => coloration bleu
			targetCell.classList.add("sea");
		} else {
			// La case correspond a un bateau => coloration rouge
			targetCell.classList.add("ship");

			// si coulé
			if(this.battleshipIsSinked(cellType)){
				this.list.querySelector(`#ship-${cellType}`).classList.add("striked");
			}

			// jeu terminé
			if(this.gameIsOver()){
				this.win_div.innerText = "Vous avez gagné !";

				const btn = document.createElement("button");
				btn.innerText = "Rejouer";

				btn.addEventListener("click", () => {
					window.location.reload();
				});

				this.win_div.appendChild(btn);
			}
		}
	}

	battleshipIsSinked(battleship)
	{
		let sinked = true;

		//Reconnaitre si un bateau a été coulé
		this.root.querySelectorAll(`td[data-typeship='${battleship}']`).forEach(cell => {
			if(!cell.classList.contains("ship"))
				sinked = false;
		});

		return sinked;
	}

	gameIsOver()
	{
		//Si la liste des bateaux = la liste des bateaux coulé
		return (this.list.querySelectorAll("li").length == this.list.querySelectorAll("li.striked").length);
	}
}

const game = new Battleship();
